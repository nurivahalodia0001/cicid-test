require('mocha')

//const { request } = require('chai');
const chai = require("chai");
const chaiHttp = require("chai-http");

chai.use(chaiHttp);

const { assert, expect, should, request } = chai;

// describe("New Array", function() {
//     describe('#length', function() {
//         it("should have value of 0", function() {
//             const array = [];
        
//             assert.equal(array.length, 0);
//             expect(array.length).to.be.an('array');
//             array.length.should.to.be.equal(0);
// //kalo TDD dia pake describe (10,11) sama it (12), kalo BDD dia pake suite sama test            

//         })
//     })
// });

should();

describe('REQRESS API', function() {
    describe('GET/unknown', function() {
        it('response status should be HTTP 200, when success (assync await)', async function() {
            const res = await request('https://reqres.in/api').get('/unknown');
            expect(res.status).to.be.equal(200);
           });

           it('response status should be HTTP 200, when success (promise)', function() {
               request('https://reqres.in/api')
               .get('/unknown')
               .end((err, res) => {
                   expect(res.status).to.be.equal(200);
               });
           });
        });
    });
    describe('POST/users', function() {
        // let Token;       *buat token login, kalo bearer gausah pake OAUTH

        // this.beforeAll(() => {

        // })
        it('response status should be HTTP 200, when success (assync await)', async function() {
            const res = await request('https://reqres.in/api').post('/users');
            expect(res.status).to.be.equal(201);
           });

           it('response status should be HTTP 200, when success (promise)', function() {
               request('https://reqres.in/api')
               .post('/users')
            // .set('Authorization', Token) *buat bearer token
            // .set('accept', 'application/yaml') *header
            // .query({
            //     page: 1
            //     size: 10
            // })       *buat param
            // .attach('image', file, 'file.png')
               .send({
                name: "morpheus",
                job: "leader"
               })
               .end((err, res) => {
                   expect(res.status).to.be.equal(201);
               });
           });
        });

        describe('PUT/users/2', function() {
            it('response status should be HTTP 200, when success (assync await)', async function() {
                const res = await request('https://reqres.in/api').put('/users/2');
                expect(res.status).to.be.equal(200);
               });
    
               it('response status should be HTTP 200, when success (promise)', function() {
                   request('https://reqres.in/api')
                   .put('/users/2')
                   .end((err, res) => {
                       expect(res.status).to.be.equal(200);
                   });
               });
            });

            describe('PUT/users/2', function() {
                it('response status should be HTTP 200, when success (assync await)', async function() {
                    const res = await request('https://reqres.in/api').put('/users/2');
                    expect(res.status).to.be.equal(200);
                   });
        
                   it('response status should be HTTP 200, when success (promise)', function() {
                       request('https://reqres.in/api')
                       .put('/users/2')
                       .end((err, res) => {
                           expect(res.status).to.be.equal(200);
                       });
                   });
                });

            